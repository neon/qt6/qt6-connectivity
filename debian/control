Source: qt6-connectivity
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Patrick Franz <deltaone@debian.org>, Lu YaNing <dluyaning@gmail.com>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libbluetooth-dev [linux-any],
               libgl-dev,
               libpcsclite-dev,
               libssl-dev,
               libvulkan-dev [linux-any],
               libxkbcommon-dev,
               mold,
               ninja-build,
               pkgconf,
               pkg-kde-tools,
               qt6-base-dev (>= 6.8.2~),
               qt6-declarative-dev (>= 6.8.2~)
Build-Depends-Indep: qt6-base-dev (>= 6.8~) <!nodoc>,
                     qt6-documentation-tools (>= 6.8~) <!nodoc>,
                     qt6-base-doc (>= 6.8~) <!nodoc>,
Standards-Version: 4.6.2
Homepage: https://www.qt.io/developers/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt6/qt6-connectivity
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt6/qt6-connectivity.git
Rules-Requires-Root: no

Package: qt6-connectivity
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: neard
Replaces: libqt6bluetooth6,
          libqt6bluetooth6-bin,
          libqt6nfc6,
Description: Qt 6 Connectivity Bluetooth library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the Bluetooth part of the Qt 6 Connectivity library.

Package: qt6-connectivity-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: qt6-base-dev,
         qt6-connectivity (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: qt6-connectivity-examples,
Description: Qt 6 Connectivity - development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header development files used for building Qt 6
 applications using Qt 6 Connectivity libraries.

Package: qt6-connectivity-doc
Architecture: all
Section: kde
X-Neon-MergedPackage: true
Depends: qt6-base-doc,
         qt6-connectivity-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: qt6-connectivity-doc-html,
Description: Qt 6 Connectivity - documentation files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header documentation files used to help build Qt 6
 applications using Qt 6 Connectivity libraries.

Package: libqt6bluetooth6
Architecture: all
Depends: qt6-connectivity, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6bluetooth6-bin
Architecture: all
Depends: qt6-connectivity, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6nfc6
Architecture: all
Depends: qt6-connectivity, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-connectivity-doc-html
Architecture: all
Depends: qt6-connectivity-doc, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-connectivity-examples
Architecture: all
Depends: qt6-connectivity-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
