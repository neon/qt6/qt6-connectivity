qt6-connectivity (6.8.2-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 09 Feb 2025 09:13:45 +1000

qt6-connectivity (6.8.1-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 08 Dec 2024 08:28:06 +1000

qt6-connectivity (6.8.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Wed, 24 Jul 2024 20:35:40 +1000

qt6-connectivity (6.7.0-0neon) noble; urgency=medium

  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sat, 17 Feb 2024 10:15:53 +1000

qt6-connectivity (6.6.1-0neon) jammy; urgency=medium

  [ Patrick Franz ]
  * New release

  [ Carlos De Maine ]
  * New release

 -- Carlos De Maine <carlosdemaine@gmail.com>  Sun, 10 Dec 2023 17:37:18 +1000

qt6-connectivity (6.4.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.4.2).
  * Bump Qt B-Ds to 6.4.2.

 -- Patrick Franz <deltaone@debian.org>  Thu, 17 Nov 2022 19:37:17 +0100

qt6-connectivity (6.4.0-2) experimental; urgency=medium

  [ Patrick Franz ]
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Tue, 18 Oct 2022 23:42:49 +0200

qt6-connectivity (6.4.0-1) experimental; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Bump minimum CMake version in order to get pkg-config's .pc files.

  [ Patrick Franz ]
  * Increase CMake verbosity level.
  * New upstream release (6.4.0).
  * Bump Qt B-Ds to 6.4.0.
  * Update list of installed files.
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Sat, 15 Oct 2022 18:42:12 +0200

qt6-connectivity (6.3.1-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Mon, 15 Aug 2022 19:22:29 +0200

qt6-connectivity (6.3.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.1).
  * Bump Qt B-Ds to 6.3.1.
  * Bump Standards-Version to 4.6.1 (no changes needed).
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Sun, 17 Jul 2022 11:42:35 +0200

qt6-connectivity (6.3.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.3.0).
  * Bump Qt B-Ds to 6.3.0.
  * Update list of installed files.
  * Update symbols from buildlogs.

 -- Patrick Franz <deltaone@debian.org>  Wed, 15 Jun 2022 00:24:21 +0200

qt6-connectivity (6.2.4-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Fri, 22 Apr 2022 20:06:29 +0200

qt6-connectivity (6.2.4-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.2.4).
  * Bump Qt B-Ds to 6.2.4.

 -- Patrick Franz <deltaone@debian.org>  Wed, 30 Mar 2022 22:00:04 +0200

qt6-connectivity (6.2.2-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Upload to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 15 Feb 2022 22:35:59 +0100

qt6-connectivity (6.2.2-1) experimental; urgency=medium

  [ Lu YaNing ]
  * Update descriptions.
  * Update symbols.

  [ Patrick Franz ]
  * New upstream release (6.2.2).
  * Bump Qt B-Ds to 6.2.2.
  * Update installed files.

 -- Patrick Franz <deltaone@debian.org>  Fri, 03 Dec 2021 16:55:23 +0100

qt6-connectivity (6.2.1-1) experimental; urgency=medium

  * Initial release (Closes: #999633)

 -- Patrick Franz <deltaone@debian.org>  Sun, 14 Nov 2021 22:58:10 +0100
